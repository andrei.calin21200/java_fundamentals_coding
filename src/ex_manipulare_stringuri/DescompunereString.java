package ex_manipulare_stringuri;

public class DescompunereString {
    public static void main(String[] args) {
        // Sa se afiseze numarul de voacale, numarul de spatii si numarul de semne de punctuatie din textul de mai jos:
        // Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
        // standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
        // It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
        // It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
        // and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

        // VOCALE - A E I O U
        String text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's";
        afisareNumarVocale(text);
        afisareNumarSpatii(text);
        afisareNumarSemnePunctuatie(text);
    }

    public static void afisareNumarVocale(String text) {
        int contor = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.toLowerCase().charAt(i);

            // if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
            //     System.out.println(c);
            // }
            switch (c) {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    // System.out.println(c);
                    contor++;
                    break;
                default:
                    // System.out.println("Este consoana");
            }
        }
        System.out.println("Numar vocale: " + contor);
    }

    public static void afisareNumarSpatii(String text) {
        String[] words = text.split(" ");
        System.out.println("Numar spatii: " + (words.length - 1));
    }

    public static void afisareNumarSemnePunctuatie(String text) {
        int contor = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
//            switch (c) {
//                case '.':
//                case ',':
//                case '?':
//                case '!':
//                case '\'':
//                    contor++;
//                    break;
//            }
            if(!(Character.isLetterOrDigit(c)|| Character.isSpaceChar(c))){
               // System.out.println("Este semn de punctuatie");
                contor++;
            }

        }
        System.out.println("Numar semne de punctuatie " + contor);
    }
}
